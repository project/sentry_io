CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers
 * Attention

INTRODUCTION
------------

Sentry.io for Drupal.

 * For a full description of the module visit:
   https://www.drupal.org/project/sentry_io

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/sentry_io

REQUIREMENTS
------------

This module requires no modules outside of Drupal core.

INSTALLATION
------------

 > Since the module requires an external library, Composer must be used.

                          composer require "drupal/sentry_io"

CONFIGURATION
-------------

  ##### Administration » Configuration » Development:

   - Add Sentry DSN

   - Select Log Levels

   - Enable Javascript (optional)


MAINTAINERS
-----------

Current maintainers:
 * Alexandre Dias (Saidatom) - https://www.drupal.org/u/saidatom

ATTENTION
---------

Most bugs have been ironed out, holes covered, features added. This module
is a work in progress. Please report bugs and suggestions, ok?
