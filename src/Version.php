<?php

namespace Drupal\sentry_io;

final class Version {
  const SDK_IDENTIFIER = 'sentry.php.drupal';
  const SDK_VERSION = '1.0.x-dev';
}
